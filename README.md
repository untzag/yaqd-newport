# yaqd-newport

[![PyPI version](https://badge.fury.io/py/yaqd-newport.svg)](https://badge.fury.io/py/yaqd-newport)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaqd-newport)](https://anaconda.org/conda-forge/yaqd-newport)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqd-newport/-/blob/master/CHANGELOG.md)

yaq daemons for Newport Corporation hardware.

This package contains two daemons:
- [newport-connex-agp](https://yaq.fyi/daemons/newport-connex-agp/)
- [newport-smc100](https://yaq.fyi/daemons/newport-smc100/)
